json.extract! home, :id, :name, :age, :gender, :created_at, :updated_at
json.url home_url(home, format: :json)